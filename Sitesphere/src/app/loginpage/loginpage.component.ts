import { Component } from '@angular/core';

@Component({
  selector: 'app-loginpage',
  templateUrl: './loginpage.component.html',
  styleUrl: './loginpage.component.css'
})
export class LoginpageComponent {
  email = '';
  password = '';

  login() {
    // Implement your login logic here
    console.log(`Email: ${this.email}, Password: ${this.password}`);
  }
}