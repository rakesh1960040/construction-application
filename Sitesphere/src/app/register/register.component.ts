import { Component } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent {
  name = '';
  email = '';
  password = '';
  confirmPassword = '';

  register() {
    // Implement your registration logic here
    console.log(`Name: ${this.name}, Email: ${this.email}, Password: ${this.password}`);
  }
}